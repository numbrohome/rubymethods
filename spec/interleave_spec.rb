require './lib/interleave'

RSpec.describe 'Ruby methods' do
  let(:array_a) { ['a', 'b', 'c'] }
  let(:array_b) { [1, 2, 3, 4, 5, 6, 7] }

  it 'merges two arrays by interleave in a one order' do
    expect(interleave(array_a, array_b)).to eq(['a', 1, 2, 'b', 3, 4, 'c', 5, 6, 7])
  end

  it 'merges two arrays by interleave in a different order' do
    expect(interleave(array_b, array_a)).to eq(['a', 1, 2, 'b', 3, 4, 'c', 5, 6, 7])
  end
end
