def interleave(a, b)
  a, b = [a, b].sort { |a, b| a.size <=> b.size }
  diff_size = (b.size/a.size).to_i.abs
  new_tab = []

  a.each_with_index do |item, index|
    new_tab.push(item)
    new_tab.push(*b.slice(index*diff_size, diff_size))
  end

  new_tab +=  b - new_tab
end
